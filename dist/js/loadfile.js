/**
 * @category   Javascript
 * @package    Onion
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2016 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-zf
 */

function include (psFilePath, psType, psMedia)
{
	var e = document.createElement(psType);
	
	if (psType = 'js')
	{
		e.type = "text/javascript";
		e.src = psFilePath;
	}
	else if (psType = 'css')
	{
		e.type = "text/css";
		e.rel = 'stylesheet';
		e.media = psMedia;
		e.href = psFilePath;
	}
	
	document.body.appendChild(e);
}

function include_once (psFilePath, psType, psMedia)
{
	var elements = document.getElementsByTagName(psType);
	
	for (var e in elements)
	{
		if (elements[e].src != null & elements[e].src.indexOf(psFilePath) != -1) 
		{
			return;
		}
	}
	
	include(psFilePath, psType);
}

function include_js (psFilePath)
{
	include_once (psFilePath, 'script');
}

function include_css (psFilePath, psMedia)
{
	include_once (psFilePath, 'link', psMedia);
}