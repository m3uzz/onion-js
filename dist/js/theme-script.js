function childMenuOpen ()
{
	var location = document.location;
	var active = null;

	$("#side-menu a").each(function(key, value){
		element = $(value);
		link = element.attr('href').replace(/^[\/]|[\/]$/g, '');
		url = location.pathname.replace(/^[\/]|[\/]$/g, '');

		rex = new RegExp(link);

		if (element.parent().hasClass('active'))
		{
			active = element;
		}
		else if (link == url)
		{
			active = element;
		}
		else if (active == null && url.match(rex))
		{
			active = element;
		}
	});

	activeMenu(active);
}

function activeMenu (element)
{
	if (element)
	{
		element.addClass('active');
		parent = element.parent().parent().addClass('in');
		parent.attr('aria-expanded', true);
		parent = parent.parent().find('a[data-toggle=collapse]');
		parent.removeClass('collapsed');
		parent.attr('aria-expanded', true);
	}
}

function hideBtnMenu ()
{
	if ($.cookie("collapse-menu") === 'hide')
	{
		$("#collapse-menu").hide();
	}
	else if ($.cookie("collapse-menu") === 'show')
	{
		$("#collapse-menu").show();
	}
}

function minMenu ()
{
	$('.menu-item-label').css('display', 'none');
	$('.main').removeClass('col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2');
	$('.main').addClass('col-sm-12 col-lg-12 main');
	$('.sidebar').removeClass('col-sm-3 col-lg-2');
	//$('.sidebar').addClass('col-sm-1 col-lg-1');
	$('.sidebar').css('width','65px');
	$("#collapse-menu").removeClass('in');
	$("#collapse-menu").addClass('out');	
	$.cookie("sidebar", 'hide', { path: '/' });
}

function hideMenu ()
{
	$("#collapse-menu-btn").html('menu');
	$('.sidebar').hide();
	$('.main').removeClass('col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2');
	$('.main').addClass('col-sm-12 col-lg-12 main');
	$("#collapse-menu").removeClass('in');
	$("#collapse-menu").addClass('out');	
	$.cookie("sidebar", 'hide', { path: '/' });
}

function showMenu ()
{
	$("#collapse-menu-btn").html('chevron_left');
	$('.main').removeClass('col-sm-12 col-lg-12');
	$('.main').addClass('col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main');
	$('.sidebar').removeClass('col-sm-1 col-lg-1');
	$('.sidebar').addClass('sidebar hidden-print collapse col-sm-3 col-lg-2');
	$('.sidebar').show();
	$('.sidebar').removeAttr('style');
	$('.menu-item-label').removeAttr('style');
	$('.sidebar').removeAttr('style');
	$("#collapse-menu").removeClass('out');
	$("#collapse-menu").addClass('in');	
	$.cookie("sidebar", 'show', { path: '/' });
}

function menuBehavior ()
{
	if ($.cookie("sidebar") === 'hide')
	{
		hideMenu();
	}
	else if ($.cookie("sidebar") === 'show')
	{
		showMenu();
	}
}

function windowSize ()
{
	showMenu ();
	
	if ($(window).width() > 768) {
		$.cookie("collapse-menu", 'show', { path: '/' });
		$("#collapse-menu").show();
		$('#sidebar-collapse').collapse('show');
	}

	if ($(window).width() <= 1024 && $(window).width() > 768) {
		$("#collapse-menu").trigger('click');
	}

	if ($(window).width() <= 767) {
		$.cookie("collapse-menu", 'hide', { path: '/' });
		$("#collapse-menu").hide();
		$('#sidebar-collapse').collapse('hide');
	}
}

$(window).on('resize', function() {
	if ($('body').attr('class') == 'default')
	{
		windowSize();
	}
});

$(function() {
	if ($('body').attr('class') == 'default')
	{
		childMenuOpen();
		menuBehavior();
		hideBtnMenu();
		
		$("#collapse-menu").off('click').on('click', function(){
			if ($(this).hasClass('in'))	{
				hideMenu();
			}
			else {
				showMenu();
			}
		});
	}

	$("ul.nav li.parent > a[data-toggle=collapse]").off("click").on("click", function() {
		$(this).find('em').toggle();
	});

	$('.sidebar a[aria-expanded=true] span[data-toggle=collapse]').find('em').toggle();	
});