/**
 * @category   Javascript
 * @package    Onion
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2016 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-zf
 */

 $.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			
			o[this.name].push(this.value || '');
		} 
		else {
			o[this.name] = this.value || '';
		}
	});
	
	return o;
 };
 
 $.fn.actFormSubmit = function(formId, containerId, fnDone, fnFail) {
	 var func = {'done':eval(fnDone), 'fail':eval(fnFail)};
	 
	 requestType = $(formId).attr('data-request');
	 responseType = $(formId).attr('data-response');
	 windowType = $(formId).attr('data-window');
	 requestMethod = $(formId).attr('method');
	 url = $(formId).attr("action");
	 params = $(formId).serializeObject();
	 
	 console.debug(formId);
	 console.debug(requestType);
	 console.debug(requestMethod);
	 console.debug(url);
	 console.debug(params);
	 
	 if (requestType == "AJAX") {
		 if (containerId != undefined)
		 {
			 $(containerId).html($("#loading").html());
		 }
		 
		 $.ajaxSetup({async:true});
		 
		 if (requestMethod.toUpperCase() == "POST" ) {
			 console.debug("AJAX POST");
			 
			 $.post(
				 url, 
				 params,
				 function( data ) {
					 console.debug(containerId);
					 
					 if (containerId != undefined)
					 {
						 console.debug(containerId);
						 $(containerId).parent().html(data);
					 }
				 },
				 "html" 
			 ).done (function( data ) {
				 console.debug('Done');
				 
				 if (fnDone != undefined)
				 {
					 func['done'](data, formId);
				 }
			 }).fail(function( data ) {
				 console.debug('Fail');
				 
				 if (fnFail != undefined)
				 {
					 func['fail'](data, formId);
				 }
			 });	
		 } 
		 else {
			 console.debug("AJAX GET");
			 
			 $.get(
				 url, 
				 params,
				 function( data ) {
					 console.debug(containerId);
					 
					 if (containerId != undefined)
					 {
						 $(containerId).parent().html(data);
					 }
				 }, 
				 "html"
			 ).done (function( data ) {
				 console.debug('Done');
				 
				 if (fnDone != undefined)
				 {
					 func['done'](data, formId);
				 }
			 }).fail(function( data ) {
				 console.debug('Fail');
				 
				 if (fnFail != undefined)
				 {
					 func['fail'](data, formId);
				 }
			 });				
		 }
		 
		 return false;
	 } 
	 else {
		 console.debug("HTML " + requestMethod);
		 
		 if (requestMethod == "GET")
		 {
			 var vars = {};
			 url.replace(/[?&]+([^=&]+)=?([^&]*)?/gi, function( m, key, value ) {
				 vars[key] = value !== undefined ? value : '';
			 });
 
			 console.debug(vars);
			 
			 if (typeof vars == 'object' && Object.keys(vars).length > 0)
			 {
				 for (key in vars)
				 {
					 $('<input>').attr({
						 type: 'hidden',
						 id: key,
						 name: key,
						 value: vars[key]
					 }).appendTo(formId);
				 }
			 }
		 }
		 
		 $(formId).submit();
	 }
 };
 
 $.fn.beep = function () {
	 var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
	 var oscillator = audioCtx.createOscillator();
	 var gainNode = audioCtx.createGain();
	 var duration = 100; //ms
	 var volume = 0.25;
	 var frequency = 230;
	 var type = 3;
 
	 oscillator.connect(gainNode);
	 gainNode.connect(audioCtx.destination);
 
	 gainNode.gain.value = volume;
	 oscillator.frequency.value = frequency;
	 oscillator.type = type;
 
	 oscillator.start();
   
	 setTimeout(
	   function(){
		 oscillator.stop();
	   }, 
	   duration
	 );  
 };
 
 $.fn.pushMessageShow = function (alertArea, messageArea, templateId, data) {
	 var qt = 0;
	 var template = '';
	 var length = 0;
 
	 if (typeof data == 'object' && Object.keys(data).length > 0)
	 {
		 length = Object.keys(data).length;
		 qt = Number($(alertArea).html()) + length;
		 $(alertArea).html(qt);
		 //$(alertArea).parent().addClass('btn-warning');
	 
		 for (key in data)
		 {
			 message = $('#pushMessage').find('#' + data[key].id);
			 console.debug(message);
			 
			 if (message != 'undefined')
			 {
				 template = $(templateId).html();
				 template = $(template).attr('id', data[key].id);
				 template = $(template).addClass('alert-' + data[key].type);
				 template = $(template).append(data[key].msg);
				 
				 if (data[key].hidden == true)
				 {
					 template = $(template).addClass('hidden');
				 }
				 
				 $(messageArea).append(template);
				 
				 $(document).off('click').on('click', '#pushMessage button', function(e){
					 msgs = $('#pushMessage').html();
					 
					 qtDisplayed = $(msgs).length;
					 
					 if (qtDisplayed > 0)
					 {
						 $(alertArea).html(qtDisplayed);
					 }
					 else
					 {
						 $(alertArea).html('');
						 $(alertArea).parent().removeClass('btn-warning');
					 }
				 });
			 }
		 }
		 
		 $(this).beep();
	 }
	 
	 return true;
 }
 
 $.fn.notificationEvent = function() {    
	 var ms = new EventSource('/backend/push');
	 
	 ms.onmessage = function (e) {
		 if (e.data != '""')
		 {
			 data = JSON.parse(e.data);
			 $(this).pushMessageShow('#pushAlert', '#pushMessage', '#pushTemplate', data);
		 }
	 };
	 
	 return ms;
 }
 
 $.fn.saveField = function (e, fnDone, fnFail){
	 var func = {'done':eval(fnDone), 'fail':eval(fnFail)};
	 var url = e.attr("data-url");
	 var id = e.attr("data-id");
	 var field = e.attr("name");
	 var value = e.val();
	 var param = "id=" + id + "&field=" + field + "&value=" + value;
	 
	 console.debug(url + param);
	 
	 $.ajaxSetup({async:true});
	 
	 $.get(
		 url,
		 param,
		 function( data ) {},
		 "html"
	 ).done (function( data ) {
		 if (fnDone != undefined)
		 {
			 func['done'](data, e);
		 }
	 }).fail(function( data ) {
		 if (fnFail != undefined)
		 {
			 func['fail'](data, e);
		 }
	 });
		 
	 return false;
 }
 
 $(function() {
	 var managerPush = null;
 
	 if (!OnionDebugJs)
	 {
		 window['console']['debug'] = function() {};
	 }
	 else
	 {
		 window['console']['debug'] = console.log;
	 }
	 
	 $(window).off('focus').on('focus', function(){
		 //console.debug('focus');
		 
		 if($('#push').length && OnionPushMsg)
		 {
			 //console.debug('start push');
			 managerPush = $(this).notificationEvent();
		 }
	 });
	 
	 $(window).off('blur').on('blur', function(){
		 //console.debug('blur');
		 
		 if($('#push').length)
		 {
			 console.debug('stop push');
			 
			 if (managerPush != null)
			 {
				 managerPush.close();
			 }
 
			 managerPush = null;
		 }
	 });
	 
	 $("#push").off('click').on('click', function(){
 
		 $('#pushMessage blockquote').removeClass('hidden');
		 $('#pushMessage').toggle();
	 });
	 
	 $(".openPopUpBtn, [data-wtype=openPopUpBtn]").off('click').on('click', function() {
		 $(this).popUpWindow(
			 $(this).attr('data-url'),
			 $(this).attr('data-params'),
			 $(this).attr('data-wname'),
			 $(this).attr('data-wwidth'),
			 $(this).attr('data-wheight'),
			 $(this).attr('data-wtop'),
			 $(this).attr('data-wleft'),
			 $(this).attr('data-wscroll'),
			 $(this).attr('data-wwindow'),
			 $(this).attr('data-reload')
		 );
 
		 return false;
	 });
	 
	 $(".openCommonModalBtn, [data-wtype=openCommonModalBtn]").off('click').on('click', function() {
		 modal = $('#commonModal');
		 modal.find(".modal-title").html($(this).attr("data-title"));
 
		 confirmBtn = modal.find("#commonModalConfirmBtn");
		 confirmBtn.html($(this).attr('data-btnName'));
		 confirmBtn.attr($(this).attr('data-pName'), $(this).attr('data-pValue'));
		 confirmBtn.attr("data-pName", $(this).attr('data-pName'));
		 confirmBtn.attr("data-form", $(this).attr('data-form'));
		 
		 if ($(this).attr('data-btnDismiss') != 'false')
		 {
			 confirmBtn.attr('data-dismiss', 'modal');	
			 confirmBtn.attr('data-bs-dismiss', 'modal');
		 }
 
		 if ($(this).attr('data-cancelBtn') == 'no')
		 {
			 modal.find("#commonModalCancelBtn").hide();
		 }
 
		 if ($(this).attr('data-header') == 'no')
		 {
			 modal.find(".modal-header").hide();
		 }
 
		 if ($(this).attr('data-footer') == 'no')
		 {
			 modal.find(".modal-footer").hide();
		 }
 
		 modalDimension = $(this).attr("data-modalDimension");
 
		 if (modalDimension != undefined && modalDimension != "")
		 {
			 dialog = modal.find('.modal-dialog');
			 dialog.removeClass(dialog.data('original-dimension'));
			 dialog.addClass(modalDimension);
			 dialog.attr('data-dimension', modalDimension);
		 }
		 
		 act = $(this).attr("data-act");
		 console.debug(act);
 
		 if (act != undefined)
		 {
			 body = modal.find(".modal-body");
			 body.html($("#loading").html());
 
			 if ($(this).attr("data-params") != '')
			 {
				 act = act + '?' + $(this).attr("data-params");
			 }
			 
			 requestMethod = $(this).attr('data-method');
			 
			 params = {'w':'modal'};
			 
			 $.ajaxSetup({async:true});
			 
			 if (requestMethod == 'post')
			 {
				 $.post(act, params, function( data ) {
					 body.html(data);
					 }, "html" 
				 );			
			 }
			 else
			 {
				 $.get(act, params, function( data ) {
					 body.html(data);
					 }, "html" 
				 );
			 }
		 }
		 
		 modal.modal('show');
	 });
	 
	 $("#commonModalConfirmBtn").off('click').on('click', function() {
		 formId = $(this).attr('data-form');
		 console.log(formId);
 
		 if (formId != undefined && formId != "")
		 {
			 form = $("#"+formId);
		 
			 if (form.has('data-act'))
			 {
				 form.attr('action', form.attr('data-act'));
			 }
		 
			 console.log($(form).attr('action'));
			 containerId = form.attr('data-container');
			 console.log(containerId);
			 returned = $(this).actFormSubmit("#"+formId, "#"+containerId);
			 console.log(returned);
			 return returned;
		 }
		 
		 return true;
	 });
	 
	 $('#commonModal').off('hidden.bs.modal').on('hidden.bs.modal', function (){
		 modal = $(this);
		 modal.find('.modal-title').html("Modal");
		 modal.find('.modal-body').html("");
		 dialog = modal.find('.modal-dialog');
		 dialog.removeClass(dialog.data('dimension'));
		 dialog.addClass(dialog.data('original-dimension'));
		 dialog.removeAttr('data-dimension');
		 confirmBtn = modal.find("#commonModalConfirmBtn");
		 confirmBtn.html(confirmBtn.data('btn-confirm-name'));
		 confirmBtn.removeAttr('data-form');
		 confirmBtn.removeAttr('data-dismiss');
		 confirmBtn.removeAttr('data-bs-dismiss');
		 confirmBtn.removeAttr(confirmBtn.data('pName'));
		 confirmBtn.removeAttr('data-pName');
		 modal.find("#commonModalCancelBtn").show();
		 modal.find(".modal-header").show();
		 modal.find(".modal-footer").show();
	 })
 });