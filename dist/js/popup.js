/**
 * @category   Javascript
 * @package    Onion
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2016 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-zf
 */

$.fn.popUpWindow = function(url, params, wName, wWidth, wHeight, wTop, wLeft, wScroll, wWindow, reloadParent){
	var wUrl;
	var wOpions;

	console.debug(url);
	console.debug(params);
	
	if (typeof (wWidth) === 'undefined')
	{
		wWidth == '600';
	}
	else if (wWidth.slice(-1) == '%')
	{
		pWidth = Number(wWidth.substr(0, wWidth.length -1));
		wWidth = Math.round((screen.width * pWidth) / 100);
	}
	
	if (typeof (wHeight) === 'undefined')
	{
		wHeight == '600';
	}
	else if (wHeight.slice(-1) == '%')
	{
		pHeight = Number(wHeight.substr(0, wHeight.length -1));
		wHeight = Math.round((screen.height * pHeight) / 100);
	}

	if (typeof (wLeft) === 'undefined' || wLeft == ''  || wLeft == null)
	{
		wLeft = Math.round((screen.width - wWidth) / 2);
	}

	if (typeof (wTop) === 'undefined' || wTop == '' || wTop == null)
	{
		wTop = Math.round((screen.height - wHeight) / 2);		
	}
	
	if(wScroll == true || wScroll == 'true')
	{
	  wScrolling = "yes";
	}
	else
	{
	  wScrolling = "no";
	}
	
	if(wWindow == "" || typeof (wWindow) === 'undefined')
	{
		wWindow = 'popup';
	}
	
	wUrl = url + '?' + params + '&w=' + wWindow;
	wOpions = '"resizable=no, scrollbars=' + wScrolling + ', width=' + wWidth + ', height=' + wHeight + ', left=' + wLeft + ', top=' + wTop + '"';
	winpopup = window.open(wUrl, wName, wOpions);

	if (reloadParent == true || reloadParent == 'true')
	{
		console.log('reload');
		var timer = setInterval(function() {
			if(winpopup.closed) {  
				clearInterval(timer);  
				window.location.reload(); 
				console.log('reload parent');
			}  
		}, 1000);
	}

	return winpopup;
}