var bgVegas = [
    { src: '/layout/vendor/m3uzz/onionjs/dist/img/trem.jpg' },
   	{ src: '/layout/vendor/m3uzz/onionjs/dist/img/salar.jpg' },
   	{ src: '/layout/vendor/m3uzz/onionjs/dist/img/flamingo.jpg' }
];

$(function() {
	$('#container').hide();

	$('body').vegas({
	    delay: 12000,
	    timer: true,
	    color: '#333333',
	    overlay: '/layout/vendor/jaysalvat/vegas/dist/overlays/07.png',
	    shuffle: false,
	    transition: 'blur2',
	    animation: 'random',
	    transitionDuration: 4000,
	    slides: bgVegas
	});
});
